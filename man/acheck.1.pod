#!/usr/bin/perl -w

=head1 NAME

acheck - Check common localization mistakes

=head1 SYNOPSIS

acheck [OPTIONS] [INPUT_FILE]

=head1 DESCRIPTION

This program parses a file checking for syntax rules and optionally asking
Aspell for checking word spelling. It makes fix suggestions and outputs a
corrected file accordingly adding review comments if requested.

It tries to find the file type according to the extension or the first lines
and loads rules accordingly.

It displays lines when they are parse.

When an error is found, a menu is displayed. Just press Enter if you don't want
to change anything. If a choice suits you, enter the corresponding number. If
you want to fix it but no choice is correct, enter a space, then you will be
asked for a string to replace the highlighted text. The script will replace the
highlighted text with your choice and parse it again for new errors.

Here are all the available commands:

=over 4

=item I<Enter>, ignore.

Ignore.

=item I<Ctrl+L>, redraw.

Rewrite the last line, suggestions and hints.

=item I<Space>, edit.

Edit the highlighted text.

=item I<E>, edit line.

Edit the whole line.

=item I<H>, add hint.

Add the displayed hint as review comment. Use this if you want the translator
to see the corresponding warning or error but you have no correction.

=item I<N>, next line.

Skip the rest of this line.

=item I<X>, exit and discard all changes.

Quit without saving modifications, the script ask you for confirmation, you
have to enter `B<yes>' to exit otherwise parsing starts again at the current
mistake.

=item I<a>, add in dictionary.

Add the highlighted word to you personal dictionary, capitalized as it is.

=item I<l>, add lowercase in dictionary.

Lowercase the highlighted word to add it to your personal dictionary.

=item I<i>, ignore word.

Ignore the highlighted word, same as I<Enter>.

=item I<I>, ignore all.

Ignore the highlighted word and add it to your session dictionary.

=back

=head1 OPTIONS

Verbosity level:

=over 4

=item I<-q>, I<--quiet>

quiet mode.

=item I<-v>

verbose, start at level I<$Debug + 1>, add more for more verbosity (see below).

=item I<--verbose n>

set verbosity level to I<n> (see below).

=back

Files:

=over 4

=item I<-i>, I<--input>

input filename, can be 'I<->' to read data from standard input.

=item I<-o>, I<--output>

output filename, can be 'I<->' to write data to standard output. If no output
filename is provided, input file is backed up with `I<bak_ext>' extension and
input filename is used.

=back

Spell check:

=over 4

=item I<-s>, I<--spell>

check spelling with Aspell.

=item I<-d language>, I<--dict language>

use I<language> dictionary for Aspell.

=item I<-n>, I<--nospell>

don't check spelling.

=back

Mode:

=over 4

=item I<-r>, I<--review>

review mode, add comments on lines beginning with I<$Comment> after parsed
line.

=item I<-t>, I<--trans>

translator mode, don't add comments, just fix errors.

=back

others:

=over 4

=item I<--rules ruleset>

use I<ruleset> rules set.

=item I<--type filetype>

use I<filetype> whatever the file type is.

=item I<--dump>

Dump the rules to check and exit, use this for debugging purposes.

=item I<-V>, I<--version>

print version and exit.

=item I<-h>, I<--help>

print a short usage message and exit.

=back

=head2 Verbosity Level

=over 4

=item I<0> quiet, normal

only warnings and errors

=item I<1> debug

names of subroutines

=item I<2> debug verbose

names and arguments of subroutines

=item I<3> .. I<5> debug very verbose

output parsing and checking details

=back

=head1 SEE ALSO

acheck(5), acheck-rules(5)

=head1 AUTHOR

Nicolas Bertolissio <bertol@debian.org>
  
=cut
