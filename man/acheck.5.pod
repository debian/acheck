#!/usr/bin/perl -w

=head1 NAME

.acheck - Acheck configuration file

=head1 DESCRIPTION

This is the configuration file for acheck. If it exists, it must be at the top
of your home directory (F<$HOME>). This file is optional unless you don't want
to use default values.

Lines beginning with a number sign (`B<#>') and empty lines will be ignored.
Spaces at the beginning and the end of a line will also be ignored as well as
tabulators.  If you need spaces at the end or the beginning of a value you can
use apostrophes (`B<">').
An option line starts with it's name followed by a value. An equal sign is
optional.
A comment starts with the number sign, there can be any number of spaces and/or
tab stops in front of the #.
Some possible examples:

 # this line is ignored
 option   value
 option = value                               # this is a comment
 option        "value ending with space   "

You have to escape number signs with a backslash (`B<\>') to use it in a value.

Default values are provided into square brackets, they should be suitable for
most installations.

=head1 SPELLING

Define if you want to use ispell for checking spelling and how to use it.

=over 4

=item I<check_spelling>

use Aspell for spelling if set to `B<yes>' [yes]

=item I<dictionary>

Language dictionary to use with Aspell, use default if empty [empty]

=item I<word_letters>

regular expression to define characters allowed to write a word, pattern matches
according locale [\w]

=item I<$review_mode>

set to `B<yes>' if you want review comments to be added in the output file
after parsed line, otherwise just fix error [no]

=back

=head1 COLORS

Set text colors for clear presentation.

The recognized colors (all of which should be fairly intuitive) are: B<clear>,
B<reset>, B<dark>, B<bold>, B<underline>, B<underscore>, B<blink>, B<reverse>,
B<concealed>, B<black>, B<red>, B<green>, B<yellow>, B<blue>, B<magenta>,
B<cyan>, B<white>, B<on_black>, B<on_red>, B<on_green>, B<on_yellow>,
B<on_blue>, B<on_magenta>, B<on_cyan>, and B<on_white>.  Case is not
significant.  Underline and underscore are equivalent, as are clear and reset,
so use whichever is the most intuitive to you.  The I<color> alone sets the
foreground color, and I<on_color> sets the background color.

Note that not all colors are supported by all terminal types, and some
terminals may not support any of these sequences.  Dark, blink, and concealed
in particular are frequently not implemented.

=over 4

=item I<error_color>

color used to highlight errors, this should highlight space characters [bold
on_red]

=item I<fix_color>

color used to highlight suggestions, this should highlight space characters
[bold on_green]

=item I<error_head_color>

color used to display the error line header [bold red]

=item I<error_color>

color used to display the suggestion line header [bold green]

=item I<comment_color>

color used for comments and hints [cyan]

=back

=head1 OTHERS

=over 4

=item I<bak_ext>

extension for backup files [bak]

=item I<comment>

comment string for review [>> ]

=item I<debug>

verbosity level [0]

=back

Verbosity Levels:

=over 4

=item I<0> quiet, normal

only warnings and errors

=item I<1> debug

names of subroutines

=item I<2> debug verbose

names and arguments of subroutines

=item I<3> .. I<5> debug very verbose

output parsing and checking details

=back

=head1 SEE ALSO

acheck(1), acheck-rules(5)

=head1 AUTHOR

Nicolas Bertolissio <bertol@debian.org>
  
=cut
