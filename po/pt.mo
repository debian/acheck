��            )         �     �  #   �     �     �               .  7   C     {     �     �     �  	   �  �   �       2   �  0   �  5   �     %     7  �  Q  	   �     �     	     !	     9	  Q   K	  C   �	  4   �	     
  L  
     g  +   v     �     �     �     �     �  Q     %   ]  !   �     �  "   �  	   �  �   �     �  9   �  4   �  F   +     r      �  :  �  	   �      �       /   ,     \  U   q  H   �  :        K                                                                                                        
                            	    %s version %s
 %s/%s:%s: `%s', unknown field name
 %s: %s %s: name %s
reserved word
 %s: spelling for `%s' %s: syntax error
 %s: value not valid
 Aspell Perl module not found, spelling check cancelled. Aspell: unable to use `%s'.
 Cannot backup `%s': %s
 Cannot read `%s': %s
 Cannot write to `%s': %s
 ERROR     Edit current line
Add hint
Skip the rest of the line
Exit and discard all changes
Add in dictionnary
Add lowercase in dictionnary
Ignore
Ignore all
 Empty file
 Enter correction to replace the highlighted part:
 Enter explanation, use displayed hint if empty:
 Exit and discard all changes! type `yes' to confirm:  Modify the line:
 Press 'Enter' to continue Usage: %s [OPTIONS] [INPUT_FILE]

options:
  -q, --quiet          quiet mode
  -v                   verbose, add more for more verbosity
      --verbose        set verbosity to n
      --rules <set>    use rules set <set>
      --type <type>    set filetype to <type>
  -i, --input <file>   input filename
  -o, --output <file>  output filename
  -s, --spell          check spelling with aspell
  -d, --dict <lang>    use <lang> dictionary
  -n, --nospell        don't check spelling
  -r, --review         add comments (for reviewers)
  -t, --trans          don't add comments (for translators)
      --dump           dump the rules and exit (for debugging)
  -V, --version        print version and exit
  -h, --help           print this message and exit

If input filename is '-' or not provided, data are read from STDIN
If output filename is not provided, input filename is used, '-' write to STDOUT

%s version %s
 WARNING   `%s': unknow offset
 `%s': unknown operation
 comment: no stop regex
 spelling for `%s' unable to resolve %s
name defined more than once as `rule', `list' and `comment'
 unable to resolve %s
undefined name as `rule', `list' or `comment'
 unable to resolve %s
undefined name as `valid' rule
 yes Project-Id-Version: acheck 0.5.1
Report-Msgid-Bugs-To: Nicolas Bertolissio <nico.bertol@free.fr>
PO-Revision-Date: 2008-05-22 08:46+0100
Last-Translator: Pedro Ribeiro <p.m42.ribeiro@gmail.com>
Language-Team: Portuguese <traduz@debianpt.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %s versão %s
 %s/%s:%s: `%s', nome de campo desconhecido
 %s: %s %s: nome %s
palavra reservada
 %s: ortografia para `%s' %s: erro de sintaxe
 %s: valor inválido
 Módulo Aspell de Perl não foi encontrado, verificação ortográfica cancelada. Aspell: não é possível usar `%s'.
 Não posso criar backup `%s': %s
 Não posso ler `%s': %s
 Não posso escrever para `%s': %s
 ERRO      Editar a linha actual
Acrescentar dica
Saltar o resto da linha
Sair e descartar todas as alterações
Acrescentar ao dicionário
Acrescentar em minúsculas ao dicionário
Ignorar
Ignorar tudo
 Ficheiro vazio
 Indicar a correcção para substituir a parte destacada:
 Indicar explicação, use a dica mostrada se vazia:
 Sair e descartar todas as alterações! escreva 'sim' para confirmar:  Modificar a linha:
 Pressione 'Enter' para continuar Utilização: %s [OPÇÕES] [FICHEIRO_ENTRADA]

opções:
  -q, --quiet          modo silencioso
  -v                   detalhado, acrescentar mais para mais detalhe
      --verbose        definir detalhe para n
      --rules <set>    usar conjunto de regras <set>
      --type <type>    definir tipo de ficheiro para <type>
  -i, --input <file>   ficheiro de entrada
  -o, --output <file>  ficheiro de saída
  -s, --spell          verificar ortografia com aspell
  -d, --dict <lang>    usar dicionário para <lang>
  -n, --nospell        não verificar ortografia
  -r, --review         acrescentar comentários (para revisores)
  -t, --trans          não acrescentar comentários (para tradutores)
      --dump           despejar as regras e sair (para depuração)
  -V, --version        mostrar versão e sair
  -h, --help           mostrar esta mensagem e sair

Se o ficheiro de entrada for '-' ou não definido, os dados são lidos de 
STDIN. Se o ficheiro de saída não for definido, é usado o ficheiro de 
entrada, se for usado '-' escreve-se para STDOUT

%s versão %s
 AVISO     '%s': deslocamento desconhecido
 `%s': operação desconhecida
 comentário: sem expressão regular de paragem
 ortografia para `%s' incapaz de resolver %s
nome definido mais de uma vez como `rule', `list' e `comment'
 incapaz de resolver %s
nome indefinido como `rule', `list' ou `comment'
 incapaz de resolver %s
nome indefinido como regra `valid'
 sim 